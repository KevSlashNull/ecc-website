import Head from 'next/head'
import Layout from '../components/layout'
import Skeleton from 'react-loading-skeleton'
import { parseISO, format } from 'date-fns'
import Link from 'next/link'
import { UserGroup } from 'heroicons-react'
import { fetch } from '../lib/fetch'
import { siteTitle } from '../lib/constants'
import { humanizeAccessLevel } from '../lib/gitlab-util'

export default function Members() {
  const { data: members } = fetch('/api/members')

  return (
    <Layout home>
      <Head>
        <title>{siteTitle} - Members</title>
      </Head>
      <section>
        <div className="flex items-center text-3xl font-bold mb-5">
          <UserGroup size="30" />
          <span className="ml-2">Members</span>
        </div>
      </section>
      <div className="flex flex-col">
        <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
              <table className="min-w-full divide-y divide-gray-200">
                <thead className="bg-gray-50">
                  <tr>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Name
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Status
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Joined at
                    </th>
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {members ? (
                    members.map(function (member, index) {
                      return (
                        <tr>
                          <td className="px-6 py-4 whitespace-nowrap">
                            <div className="flex items-center">
                              <div className="flex-shrink-0 h-10 w-10">
                                <img className="h-10 w-10 rounded-full" src={member.avatar_url} alt={member.name} />
                              </div>
                              <div className="ml-4">
                                <Link href={member.web_url}>
                                  <a className="text-sm font-medium text-gray-900" target="_blank">
                                    {member.name}
                                  </a>
                                </Link>
                                <div className="text-sm text-gray-500">@{member.username}</div>
                              </div>
                            </div>
                          </td>
                          <td className="px-6 py-4 whitespace-nowrap">
                            <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                              {humanizeAccessLevel(member.access_level)}
                            </span>
                          </td>
                          <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                            {format(parseISO(member.created_at), 'LLLL d, yyyy')}
                          </td>
                        </tr>
                      )
                    })
                  ) : (
                    <MembersSkeleton />
                  )}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}

/**
 * Used as loading skeleton in the table
 */
function MembersSkeleton() {
  const height = 30

  return Array(10)
    .fill(1)
    .map(() => (
      <tr>
        <td className="py-5 px-3">
          <Skeleton height={height} />
        </td>
        <td className="py-5 px-3">
          <Skeleton height={height} />
        </td>
        <td className="py-5 px-3">
          <Skeleton height={height} />
        </td>
      </tr>
    ))
}
