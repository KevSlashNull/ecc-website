## Contributing

We expect contributors to abide by our underlying
[code of conduct](CODE_OF_CONDUCT.md). All conversations and
discussions on GitLab (issues, merge requests) and across
[EveryoneCanContribute](https://everyonecancontribute.com) must be respectful and harassment-free.

If you feel another member of the community has violated our Code of Conduct,
you may anonymously contact the team with our
[abuse report form](https://forms.gle/2ioc8AY8Cp8KyL8F7).

Remember that communication is the lifeblood of any Open Source project. We are
all working on this together, and we are all benefiting from this software. It's
very easy to misunderstand one another over asynchronous, text-based
conversations: When in doubt, assume everyone within this project has the best
intentions.

We are all humans trying to work together to improve the community. Always be
kind and appreciate the need for trade-offs. ❤️
