import React, { useState } from 'react'
import Head from 'next/head'
import Link from 'next/link'

export default function Navbar({ children, home }) {
  const [toggleMobile, setToggleMobile] = useState(true)

  return (
    <nav className="bg-gray-900">
      <div className="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">
        <div className="relative flex items-center justify-between h-16">
          <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
            <button
              onClick={() => setToggleMobile(!toggleMobile)}
              type="button"
              className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
              aria-controls="mobile-menu"
              aria-expanded="false"
            >
              <span className="sr-only">Open main menu</span>
              <svg
                className="block h-6 w-6"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                aria-hidden="true"
              >
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16" />
              </svg>
              <svg
                className="hidden h-6 w-6"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                aria-hidden="true"
              >
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12" />
              </svg>
            </button>
          </div>
          <div className="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
            <div className="flex-shrink-0 flex items-center">
              <Link href="/">
                <a className="text-white text-xl font-bold">EveryoneCanContribute</a>
              </Link>
              <span className="bg-gradient-to-r font-mono font-bold from-purple-700 ml-3 px-2 rounded-2xl text-white to-purple-800">
                Next
              </span>
            </div>
          </div>
          <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
            <div className="hidden sm:block sm:ml-6">
              <div className="flex space-x-4">
                <a
                  href="#"
                  className="text-gray-300 hover:bg-gray-700 hover:text-white focus:bg-gray-700 focus:text-white px-3 py-2 rounded-md text-sm font-medium"
                >
                  About
                </a>
                <Link href="/members">
                  <a className="text-gray-300 hover:bg-gray-700 hover:text-white focus:bg-gray-700 focus:text-white px-3 py-2 rounded-md text-sm font-medium">
                    Members
                  </a>
                </Link>
                <a
                  href="#"
                  className="text-gray-300 hover:bg-gray-700 hover:text-white focus:bg-gray-700 focus:text-white px-3 py-2 rounded-md text-sm font-medium"
                >
                  Handbook
                </a>
                <IconLink href="#" icon="/podcast.svg"></IconLink>
                <IconLink href="#" icon="/twitter.svg"></IconLink>
                <IconLink href="#" icon="/youtube.svg"></IconLink>
                <IconLink href="/discord" icon="/discord.svg"></IconLink>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className={`sm:hidden ${toggleMobile && 'hidden'}`}>
        <div className="px-2 pt-2 pb-3 space-y-1">
          <a href="#" className="bg-gray-900 text-white block px-3 py-2 rounded-md text-base font-medium">
            Dashboard
          </a>
          <a
            href="#"
            className="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium"
          >
            Team
          </a>
          <a
            href="#"
            className="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium"
          >
            Projects
          </a>
          <a
            href="#"
            className="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium"
          >
            Calendar
          </a>
        </div>
      </div>
    </nav>
  )
}

function IconLink({ href, icon }) {
  return (
    <Link href={href}>
      <a className="hover:bg-gray-700 focus:bg-gray-700 px-3 py-2 rounded-md flex">
        <img className="w-5" src={icon} />
      </a>
    </Link>
  )
}
