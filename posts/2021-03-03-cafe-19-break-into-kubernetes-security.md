---
title: "19. Cafe: Break into Kubernetes Security"
date: "2021-03-03"
Aliases: []
Tags: ["gitlab","hetzner","cloud","terraform","ansible","kubernetes",""]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

We are learning how to deploy and secure Kubernetes into Hetzner cloud in this series:

- 14. cafe: Provisioned the server and agent VMs with Terraform and Ansible [in the first session](/post/2021-01-27-cafe-14-kubernetes-deployments-to-hetzner-cloud/) with [Max](https://twitter.com/ekeih).
- 15. cafe: Deployed [k3s as Kubernetes distribution](/post/2021-02-03-cafe-15-kubernetes-deployments-to-hetzner-cloud-part-2/) with [Max](https://twitter.com/ekeih).
- 16. cafe: Learned about pods and the [Hetzner load balancer](/post/2021-02-10-cafe-16-kubernetes-deployments-to-hetzner-cloud-part-3/) with [Max](https://twitter.com/ekeih).
- 17. cafe: [Ingress controller for load balancer cost savings](/post/2021-02-17-cafe-17-kubernetes-deployments-to-hetzner-cloud-part-4/) with [Max](https://twitter.com/ekeih).
- 18. cafe: [Get to know Kubernetes user authentication and authorization with RBAC](/post/2021-02-24-cafe-18-kubernetes-rbac-authentication-authorization/) with [Niclas Mietz](https://twitter.com/solidnerd).

In this session, we change the perspective and try to break into a Kubernetes cluster with [Philip Welz](https://twitter.com/philip_welz).

* Scenario: view access to namespace `everyonecancontribute` & edit access to namespace `philips-workspace`
* First, we check the permissions in the namespace with `kubectl auth can-i --list`
* Inspect all namespaces and pods to fetch as much information as possible `kubectl get pods -o yaml |grep secret -A5 -B5`
* Then create a new pod with [privileged mode](https://kubernetes.io/docs/concepts/policy/pod-security-policy/#privileged) and execute a bash session into it
* Mount the host filesystem, download kubectl from the internet and use [kubelet.conf](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/kubelet-integration/#the-kubelet-drop-in-file-for-systemd) as our `KUBECONFIG`
* Gather as much as possible information about the Cluster ( Nodes, IPs, etc. )
* Generate an SSH key, move it to the mounted host filesystem and access the host as root
* Use container runtime CLI `crictl` to retrieve passwords 
* [Authenticate against the Kubernetes API](https://kubernetes.io/docs/tasks/administer-cluster/access-cluster-api/#directly-accessing-the-rest-api) and fetch the remaining tokens/passwords.
* Created again a pod with [privileged mode](https://kubernetes.io/docs/concepts/policy/pod-security-policy/#privileged) but this time scheduled it with [nodeName](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#nodename) to one control plane node
* Mount the host filesystem again and use the [admin.conf](https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-config/) as our `KUBECONFIG` to gain full cluster admin rights
* [Target etcd](https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/#before-you-begin) to read the secrets in plaintext.
* Defend tactics
  * Use [Pod Security Policies](https://kubernetes.io/docs/concepts/policy/pod-security-policy/). They are deprecated in favour of [Kyverno](https://kyverno.io/) or [Open-Policy-Agent](https://www.openpolicyagent.org/docs/v0.12.2/kubernetes-admission-control/)

Next week, we'll look into more security topics and more:

- OpenID Connection of the API Server with Dex and GitLab, continued. 
- Hetzner [storage volumes](https://github.com/hetznercloud/csi-driver)
- Future ideas touch monitoring with Prometheus, GitLab CI/CD deployments and much more :) 

### Insights

- [Kubernetes group repos](https://gitlab.com/everyonecancontribute/kubernetes)
- [Repository with all commands from the session](https://gitlab.com/everyonecancontribute/kubernetes/k8s-security/-/tree/main/session-1)
- [Twitter thread](https://twitter.com/dnsmichi/status/1367107280769060873)
- [KubeCon 2019 CTF](https://securekubernetes.com)
- [Attacking Kubernetes through Kubelet](https://labs.f-secure.com/blog/attacking-kubernetes-through-kubelet)
- [BadPods examples & explanations](https://github.com/BishopFox/badPods)
- [deploy a pod that gives us full host access](https://twitter.com/mauilion/status/1129468485480751104)
- [kubectl node shell](https://github.com/kvaps/kubectl-node-shell)

### Recording

Enjoy the session! 🦊 

{{< youtube nOTIRo2-atU >}}






